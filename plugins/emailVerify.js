import { auth } from '~/firebase'
import { SET_VERIFICATION } from '~/helpers/types/user.js'

export default function({ store, router }) {
  auth.onAuthStateChanged((user) => {
    if (user) {
      store.commit(`user/${SET_VERIFICATION}`, user.emailVerified)
    }
  })
}
