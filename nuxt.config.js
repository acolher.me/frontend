export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Acolher.me - Atraia, gerencie e engaje doadores',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Gerencie e capte recursos online pontuais e recorrentes através de boleto, cartão de crédito e débito em conta.'
      },
      {
        name: 'og:locale',
        content: 'pt_BR'
      },
      {
        name: 'og:type',
        content: 'website'
      },
      {
        name: 'og:title',
        content: 'Acolher.me - Atraia, gerencie e engaje doadores'
      },
      {
        name: 'og:description',
        content:
          'Gerencie e capte recursos online pontuais e recorrentes através de boleto, cartão de crédito e débito em conta.'
      },
      {
        name: 'og:url',
        content: 'https://acolher.me'
      },
      {
        name: 'og:site_name',
        content: 'Acolher.me'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap'
      },
      {
        rel: 'canonical',
        href: 'https://acolher.me/'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#4B45BD', height: '5px' },
  /*
   ** Global CSS
   */
  css: [
    { src: 'ant-design-vue/dist/antd.less', lang: 'less' },
    { src: 'animate.css/animate.min.css', lang: 'css' },
    { src: '~/assets/style.less', lang: 'less' }
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/antd-ui',
    '~/plugins/emailVerify',
    { src: '@/plugins/aos', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  devModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    'vue-scrollto/nuxt'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  pwa: {
    manifest: {
      name: 'Acolher.me',
      description: 'Acolher.me - Atraia, gerencie e engaje doadores',
      lang: 'pt-BR',
      theme_color: '#4B45BD'
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    },
    loaders: {
      less: {
        modifyVars: {
          'primary-color': '#4B45BD',
          'secondary-color': '#58CAEA',
          'third-color': '#BAC5F4',
          'link-color': '#4B45BD',
          'layout-sider-background-light': '#F8F8FF',
          'menu-bg': '#F8F8FF',
          'font-family':
            "'Montserrat', Roboto, 'Helvetica Neue', Helvetica, -apple-system, BlinkMacSystemFont, 'Segoe UI', Arial, sans- serif"
        },
        javascriptEnabled: true
      }
    }
  }
}
