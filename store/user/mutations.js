import { SET_USER, SET_UID, SET_VERIFICATION } from '~/helpers/types/user.js'

export default {
  [SET_USER](state, user) {
    console.log('[MUTATIONS] - SET_USER:', user)
    state.data = user
  },
  [SET_UID](state, uid) {
    console.log('[MUTATIONS] - SET_UID:', uid)
    state.uid = uid
  },
  [SET_VERIFICATION](state, isVerified) {
    console.log('[MUTATIONS] - SET_VERIFICATION:', isVerified)
    state.isVerified = isVerified
  }
}
