import Cookies from 'js-cookie'
import { SET_USER, SET_UID, SET_VERIFICATION } from '~/helpers/types/user.js'
import { auth } from '~/firebase'

export default {
  async login({ commit }, user) {
    console.log('[ACTIONS] - Logging user')

    const userInfo = {
      displayName: user.displayName,
      photoURL: user.photoURL,
      email: user.email
    }

    Cookies.set('access_token', user.token)
    await commit(SET_USER, userInfo)
    await commit(SET_UID, user.uid)
    await commit(SET_VERIFICATION, user.isVerified)
  },

  async logout({ commit }) {
    console.log('[STORE ACTIONS] - logout')
    await auth.signOut()

    Cookies.remove('access_token')
    commit(SET_USER, null)
    commit(SET_UID, null)
    commit(SET_VERIFICATION, null)
  }
}
