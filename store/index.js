import { SET_USER, SET_UID, SET_VERIFICATION } from '~/helpers/types/user.js'
import { getUserFromCookie } from '~/helpers/user'

export const actions = {
  nuxtServerInit({ commit }, { req }) {
    const user = getUserFromCookie(req)
    if (user) {
      commit(`user/${SET_USER}`, {
        displayName: user.name,
        email: user.email,
        photoURL: user.picture
      })

      commit(`user/${SET_VERIFICATION}`, user.email_verified)

      commit(`user/${SET_UID}`, user.user_id)
    }
  }
}
