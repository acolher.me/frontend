import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_MESSAGING_SENDER_ID
}

const { TimeStamp, GeoPoint, FieldValue } = firebase.firestore
const googleProvider = new firebase.auth.GoogleAuthProvider()

const app = !firebase.apps.length
  ? firebase.initializeApp(config)
  : firebase.app()

export default app

const db = app.firestore()
const auth = app.auth()
export { db, auth, googleProvider, TimeStamp, GeoPoint, FieldValue }
