// Middleware for SignIn and SignUp pages
export default function({ store, redirect }) {
  const isLogged = !!store.state.user && !!store.state.user.uid
  const isVerified = !!store.state.user && !!store.state.user.isVerified
  if (isLogged) {
    if (isVerified) {
      redirect('/admin')
    } else {
      redirect('/auth/email-verification')
    }
  }
}
