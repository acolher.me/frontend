// Middleware for Admin pages
export default function({ store, redirect }) {
  if (process.env.NODE_ENV === 'production') {
    const isLogged = !!store.state.user && !!store.state.user.uid
    const isVerified = !!store.state.user && !!store.state.user.isVerified
    if (isLogged) {
      if (!isVerified) {
        return redirect('/auth/email-verification')
      }
    } else {
      return redirect('/auth/signin')
    }
  }
}
