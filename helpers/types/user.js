export const SET_USER = 'SET_USER'
export const SET_UID = 'SET_UID'
export const SET_VERIFICATION = 'SET_VERIFICATION'
export const LOGIN_START = 'LOGIN_START'
